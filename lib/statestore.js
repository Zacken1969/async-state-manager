

module.exports = {
    MQTT: require("./statestore.mqtt"),
    Kafka: require("./statestore.asynckafka"),
    Prototype: require("./statestore.prototype")
}
