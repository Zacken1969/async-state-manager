const EventEmitter = require('events');
const async = require("async");
const StateStore = require("./statestore");
const shared = require("./shared");
const clone = require("clone");
const deepmerge = require("deepmerge");

class State extends EventEmitter {

    stateStoreType = null;
    persistTimeoutMs = null;
    stateStore = null;
    state = new Map();

    persistState = null;
    filter = null;
    mapper = null;
    handler = null;
    cleaner = null;
    cleanerIntervalMs = null;
    cleanerTimeout = null;
    connected = false;

    //processRecordsAfterLoaded=null;



    constructor(options) {
        super();
        this.stateStoreType = options.stateStoreType || "kafka";
        this.persistState = options.persistState || false;
        this.persistTimeoutMs = options.persistTimeoutMs || 2000;
        this.filter = options.filter || null;
        this.mapper = options.mapper || null;
        this.changeHandler = options.changeHandler || null;
        this.cleaner = options.cleaner || null;
        this.cleanerIntervalMs = options.cleanerIntervalMs || null;
        //this.handleChangesAfterLoaded = options.processRecordsAfterLoaded || null;

        const self = this;
        async function recordProcessor(key, record, type, persistToken) {
            //console.log(type.toUpperCase() + ": (" + key + ") " + JSON.stringify(record));

            let handle = true;

            if (self.filter && !self.persistState) {
                handle = await self.filter(key, record, type);
            }
            if (handle) {
                if (self.mapper && !self.persistState) {
                    let result = await self.mapper(key, record, type);
                    if (result.key) key = result.key;
                    if (result.record) record = result.record;
                    if (result.type) type = result.type;
                }

                if (!persistToken) {
                    if (type == "delete")
                        self.state.delete(key);
                    else
                        self.state.set(key, record);
                }


                if (self.changeHandler) {
                    await self.changeHandler(key, record, type);
                }
            }
        }


        if (this.stateStoreType.toLowerCase() == "kafka") {
            if (options.kafka) this.stateStore = new StateStore.Kafka(options, recordProcessor);
            else throw ("Kafka state store need options.kafka to be set")
        }
        else if (this.stateStoreType.toLowerCase() == "mqtt") {
            if (options.mqtt) this.stateStore = new StateStore.MQTT(options, recordProcessor);
            else throw ("MQTT state store need options.mqtt to be set")
        }
        else throw ('Unknown stateStoreType. "mqtt" and "kafka" are valid types')

    }


    length() {
        return this.state.length();
    }


    async connect() {
        if (!this.connected) {
            this.connected = true;
            await this.stateStore.start();
            if (this.cleanerIntervalMs) {
                await this.clean();
            }
        }
    };

    async disconnect() {
        if (this.connected) {
            this.connected = false;
            clearTimeout(this.cleanerTimeout);
            await this.stateStore.stop();
        }
    };

    async clean() {
        if (this.cleaner) {
            console.log("STARTING CLEANING CYCLE")
            clearTimeout(this.cleanerTimeout);
            let removedCount = 0;
            await async.eachLimit(this.state, 1, async (record) => {
                const keep = await this.cleaner(record[0], record[1]);
                if (!keep) {
                    //console.log("DELETING: "+record[0])
                    await this.delete(record[0]);
                    //await new Promise((resolve, reject) => {
                    //    setImmediate(resolve());
                    //})
                    removedCount++;
                }

            });
            if (this.cleanerIntervalMs) {
                this.cleanerTimeout = setTimeout(() => { this.clean() }, this.cleanerIntervalMs);
            }
            console.log("CLEANED: " + removedCount)
        }
    }

    async merge(key, record) {
        if (this.persistState) {
            let stateRecord = record
            let oldRecord = this.state.get(key);
            if (oldRecord) {
                const overwriteMerge = (destinationArray, sourceArray, options) => sourceArray;
                stateRecord = deepmerge(oldRecord, record, { arrayMerge: overwriteMerge });
                //logger.silly("Merged state with old state for key: " + this.stateName + "/" + stateKey);
            }
            await this.update(key, stateRecord);

        } else throw ("merge(): This state is not setup to persist");
    };

    async update(key, record) {
        if (this.persistState) {
            let oldRecord = this.state.get(key);
            if (oldRecord) {
                let equal = shared.isObjectEqual(oldRecord, record);
                if (!equal) {
                    await this.set(key, record);
                }

            } else {
                await this.set(key, record);
            }

        } else throw ("update(): This state is not setup to persist");
    };

    async set(key, record) {
        if (this.persistState) {
            if (!record)
                this.state.delete(key);
            else
                this.state.set(key, record);
            //await new Promise((resolve, reject) => {
            // const timeout = setTimeout(() => {
            //    throw("Persisting record timed out (" + this.persistTimeoutMs + "ms)")
            //}, this.persistTimeoutMs);
            await this.stateStore.persist(key, record);//, (key, record, persistToken) => {
            // clearTimeout(timeout);
            //resolve();
            //});
            //});
        } else throw ("set(): This state is not setup to persist");
    };
    async delete(key) {
        if (this.persistState) {
            if (await this.has(key))
                await this.set(key, null);
        } else throw ("delete(): This state is not setup to persist");
    };

    async clear() {
        if (this.persistState) {
            await this.stateStore.clear();
            await this.stateStore.purge();
            this.state = new Map();
        } else throw ("clear(): This state is not setup to persist");
    };

    async get(key) {
        return this.state.get(key);
    };

    async has(key) {
        return this.state.has(key);
    };

}

module.exports = State;
