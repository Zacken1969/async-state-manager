const EventEmitter = require('events');
const async = require("async");

class StateStore extends EventEmitter {

    persistState = null;
    persistToken = 1;
    persistQueue = null;
    recordQueue = null;
    //filter=null;
    //mapper=null;
    //processor=null;
    recordProcessor = null;
    processRecordsAfterLoaded = null;
    loaded = false;
    persistingRecords = new Map();
    recordKeys = new Map();
    started = false;
    autoConnect=null;

    maxQueue = 500;

    //Overide this and call super()
    constructor(options, recordProcessor) {
        super();
        this.persistState = options.persistState || false;
        this.processRecordsAfterLoaded = options.processRecordsAfterLoaded || false;
        this.autoConnect = options.autoConnect || false;

        if (recordProcessor) this.recordProcessor = recordProcessor;
        else this.recordProcessor = options.recordProcessor || null;

        this.persistQueue = async.queue(async (persistCommand) => {
            await this._processPersistCommand(persistCommand.key, persistCommand.record, persistCommand.persistToken);
        }, 1);

        this.recordQueue = async.queue(async (record) => {
            if (this.recordProcessor) await this.recordProcessor(record.key, record.record, record.type, record.persistToken);
            this.emit("record", record.key, record.record);
        }, 1);
    }

    async start() {
        if (!this.started) {
            this.started = true;
            console.log("Starting state store driver and loading state");
            if(this.autoConnect) await this._connect();
            const loadedPromise = new Promise((resolve, reject) => {
                this.once("loaded", () => {
                    resolve();
                });
            });
            const subscribePromise = this._subscribe();

            await Promise.all([loadedPromise, subscribePromise]);
            console.log("State store driver and state loaded");
            //this.loaded = true;
            if (this.persistState) {
                console.log("Stop state store driver to start persisting");
                await this._unsubscribe();
            }
        }
    }

    async stop() {
        if (this.started) {
            this.started = false;
            console.log("Stoping state store driver");
            await this._unsubscribe();
            //if (this.persistState) 
            await this.purge();
            if(this.autoConnect) await this._disconnect();
            this.recordKeys = new Map();
            console.log("State store driver stopped");
        }
    }

    async purge() {
        console.log("Purging state store driver");
        if (this.persistState)
            await async.whilst(
                async () => this.persistingRecords.size,
                async () => {
                    await new Promise((resolve, reject) => setImmediate(() => resolve()));
                });

        await this._purge();

        await async.whilst(
            async () => this.recordQueue.length(),
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });
        console.log("State store driver purged");
    }

    async clear() {
        await async.eachOfLimit(this.recordKeys, 1,
            async (record) => {
                await this.persist(record[0], null);
            });
        this.recordKeys = new Map();
    }

    async persist(key, record, persistedCallback) {
        if (this.started) {
            if (!this.persistState) throw ("Cannot purge a state store that does not allow persistence");
            this.persistToken++;
            const persistCommand = {
                persistToken: this.persistToken,
                key: key,
                record: record,
                persistedCallback: persistedCallback
            }
            this.persistingRecords.set(persistCommand.persistToken, persistCommand);
            if (this.persistQueue.length() >= this.maxQueue) {
                //console.log("Queue full")
                //await new Promise((resolve,reject)=>{
                //    setImmediate(resolve());
                //})
                //console.log("Yielded")
                await this.persistQueue.drain();
                //console.log("Queue drained")
            }
            this.persistQueue.push(persistCommand);

            return this.persistToken;
        }
    }

    //Call from storage driver to queue incomming record update/insert/delete
    async _queueRecord(key, record, persistToken) {

        let type = "unknown";
        if (!record) {
            type = "delete";
            this.recordKeys.delete(key);
        }
        else if (this.recordKeys.has(key)) {
            type = "update";
        } else {
            type = "insert";
            this.recordKeys.set(key, key);
        }

        if (!this.loaded || (this.loaded && this.processRecordsAfterLoaded)) {
            const message = {
                key: key,
                record: record,
                type: type,
                persistToken: persistToken
            }
            this.recordQueue.push(message);
        }
    }

    //Call from storage driver when message is persisted to storage
    async _recordPersisted(persistToken) {
        const persistCommand = this.persistingRecords.get(persistToken);
        if (!persistCommand) throw ("Storagedrive is trying to mark non existent record as persisted");
        await this._queueRecord(persistCommand.key, persistCommand.record, persistCommand.persistToken);
        if (persistCommand.persistedCallback) await persistCommand.persistedCallback(persistCommand.key, persistCommand.record, persistCommand.persistToken);
        this.persistingRecords.delete(persistToken);
    }

    //Call from storage driver when current state has been loaded and queued
    async _loaded() {
        await async.whilst(
            async () => this.persistQueue.length(),
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });
        await async.whilst(
            async () => this.recordQueue.length(),
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });
        console.log("All records loaded from state store")
        this.loaded = true;
        this.emit("loaded");
    }

    //Override these and do not call super();
    //Write persisted messages to store, Could be buffered. Buffer is written with _purge()
    async _processPersistCommand(key, record, persistToken) {
        console.log("Dummy _processPersistCommand() called. No driver configured. Not persisting.");
        await this._recordPersisted(persistToken);
    }

    //Flush unwritten records to store
    async _purge() {
        console.log("Dummy _purge() called. No driver configured. Nothing to purge.");

    }

    //Connect and subscribe to store. Pull or push. Setup timers
    async _subscribe() {
        console.log("Dummy _subscribe() called. No driver configured.");
        await this._loaded();
    }

    //Unsubscribe and disconnect from store. Clear timers.
    async _unsubscribe() {
        console.log("Dummy _unsubscribe() called. No driver configured.");
    }

    async _connect() {
        console.log("Dummy _connect() called. No driver configured.");
    }

    async _disconnect() {
        console.log("Dummy _disconnect() called. No driver configured.");
    }

}

module.exports = StateStore;