const StateStore = require("./statestore.prototype");
const async = require("async");
const shared = require("./shared");

class StateStoreKafka extends StateStore {

    kafkaProducer = null;
    kafkaConsumer = null;
    kafkaTopic = null;

    consumerWasConnected = false;
    producerWasConnected = false;
    isSubscribed = false;

    //Overide this and call super()
    constructor(options, recordProcessor) {
        super(options, recordProcessor);

        this.persistState = options.persistState || false;

        if (!options.kafka.producer && this.persistState) throw ("State store (Async-Kafka) need a producer object to persist state");
        if (!options.kafka.consumer) throw ("State store (Async-Kafka) need a producer object to read state");
        if (!options.kafka.topic) throw ("State store (Async-Kafka) need a state topic to identify state store");
        this.kafkaProducer = options.kafka.producer;
        this.kafkaConsumer = options.kafka.consumer;
        this.kafkaTopic = options.kafka.topic;

    }

    //Override these and do not call super();
    //Write persisted messages to store, Could be buffered. Buffer is written with _purge()
    async _processPersistCommand(key, record, persistToken) {
        await this.kafkaProducer.send(this.kafkaTopic,
            record,
            key,
            async () => {
                await this._recordPersisted(persistToken)
            },
            2
        );
    }

    //Flush unwritten records to store
    async _purge() {

    }

    //Connect and subscribe to store. Pull or push. Setup timers
    async _subscribe() {
        if (!this.isSubscribed) {

            await this.kafkaConsumer.subscribe([{
                topic: this.kafkaTopic,                           //Name of topic on broker
                decoderType: 2,             //Can be of type AUTO, JSON, AVRO, STRING and BUFFER
                consumeMode: 1, //Available modes EARLIEST, LAST_COMMIT, LATEST and NEXT
                autoCommit: false,                                   //If TRUE topic will be commitet minimum by the intervall specified in consumer property 'commitIntervalMs' and to latest processed message on unassign
                keepOffsetOnReassign: true,                        //If TRUE consumer will continue from after last processed message if partition is reassigned even if not commited
                processor: async (record, kafkaHeader) => {
                    this._queueRecord(kafkaHeader.key, record);
                }
            }]);
            this.isSubscribed = true;
            await this._loaded();
        }
    }

    //Unsubscribe and disconnect from store. Clear timers.
    async _unsubscribe() {
        if (this.isSubscribed) {
            this.isSubscribed = false;
            await this.kafkaConsumer.unsubscribe([this.kafkaTopic]);
        }
    }

    async _connect() {
        if (!this.kafkaConsumer.isConnected()) await this.kafkaConsumer.connect();
        else this.consumerWasConnected = true;
        if (this.persistState) {
            if (!this.kafkaProducer.isConnected())
                await this.kafkaProducer.connect();
            else this.producerWasConnected = true;
        }
    }


    async _disconnect() {
        if (this.kafkaConsumer.isConnected() && !this.consumerWasConnected)
            await this.kafkaConsumer.disconnect();
        if (this.persistState) {
            if (this.kafkaProducer.isConnected() && !this.producerWasConnected)
                await this.kafkaProducer.disconnect()
        }
    }

}

module.exports = StateStoreKafka;