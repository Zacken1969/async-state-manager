const StateStore = require('./statestore');
const async = require("async");
const shared = require("./shared");

class StateStoreJSON extends StateStore {

    #file = null;
    #pushIntervalSec = null;
    #pushTimeout = null;
    #lastPolledKeys = [];
    #waitingChanges = [];
    #persistToken = 0;


    constructor(options) {
        super(options);

        //Validate StateStoreJSON options
        if (!options.file) throw ("State store (JSON) need file options to be set to construct object");

        this.#file = options.file;
        this.#pushIntervalSec = options.pushIntervalSec || 10;



    };

    async load() {
        await super.load([]);
        return await this.#pollFile("LOAD");
    };

    async persist(key, entityRecord) {
        const persistToken = this.#persistToken;
        this.#persistToken++;
        this.#waitingChanges.push({ key: key, entity: entityRecord, persistToken: persistToken });
        return { key: key, persistToken: persistToken };;
    };

    async clear() {        
        const fs = require('fs/promises');
        await fs.writeFile(this.#file, "{}", {});
        return await super.clear();
    }

    async close() {
        const result= await super.close();
        if (this.#pushTimeout) {
            clearTimeout(this.#pushTimeout);
            this.#pushTimeout = null;
        }
        return result;
    };

    async purge() {
        return await this.#purgeToFile()
    };

    async #purgeToFile() {
        if (this.#pushTimeout) {
            clearTimeout(this.#pushTimeout);
            this.#pushTimeout = null;
        }

        const changes = this.#waitingChanges;
        this.#waitingChanges = [];

        const count = changes.length
        if (count) {
            const entities = await this.#getEntitiesFromFile();
            await async.eachOfLimit(changes, 1,
                async (change) => {
                    if (change.entity) entities[change.key] = change.entity;
                    else delete entities[change.key];
                });
            const fs = require('fs/promises');
            const jsonString = JSON.stringify(entities);

            await fs.writeFile(this.#file, jsonString, {});

            await async.eachOfLimit(changes, 1,
                async (change) => {
                    this.emit("persisted", { key: change.key, persistToken: change.persistToken });
                    await super.recieve(change.key, change.entity);
                });
        }
        this.#pushTimeout = setTimeout(() => this.#purgeToFile(), this.#pushIntervalSec * 1000);
        return count;
    }


    async #pollFile(pollType) {

        const polledKeys = [];
        const entities = await this.#getEntitiesFromFile();
        await async.eachOfLimit(Object.entries(entities), 1,
            async (entity) => {
                await super.recieve(entity[0], entity[1]);
                polledKeys.push(entity[0]);
            });

        if (this.#lastPolledKeys.length && polledKeys.length && (pollType === "UPDATE")) {
            await async.eachOfLimit(this.#lastPolledKeys, 1,
                async (key) => {
                    await super.recieve(key, entity);
                    polledKeys.push(key);
                });



        }
        this.#lastPolledKeys = polledKeys;
        return polledKeys.length;
    }

    async #getEntitiesFromFile() {
        const fs = require('fs/promises');
        try {
            return JSON.parse(await fs.readFile(this.#file));
        } catch {
            return {};
        }

    }
}

module.exports = StateStoreJSON;