function isObject(item) {
    return (typeof item === "object" && !Array.isArray(item) && item !== null);
}

const diff = require("deep-object-diff").diff;
function isObjectEqual(oldObject, newObject) {
    let changes = diff(oldObject, newObject);
    if (Object.keys(changes).length === 0) return true;
    else return false;
}

function isArrayOfObject(item) {
    if (isArray(item)) {
        item.forEach(object => {
            if (!isObject(object)) return false;
        });
        return true;
    } else return false;
}

function isArray(item) {
    return Array.isArray(item);
}

function isOlderThan(timestamp, age) {
    const now = new Date();
    const then = timestamp;
    const dateDiff = now - then;
    if (dateDiff > age) return true;
    else return false;
}

module.exports = {
    isObject: isObject,
    isArrayOfObject: isArrayOfObject,
    isArray: isArray,
    isOlderThan:isOlderThan,
    isObjectEqual:isObjectEqual
}