const StateStore = require("../lib/statestore.prototype");
const MqttClient = require("mqtt");
const async = require("async");
const shared = require("./shared");

class StateStoreMQTT extends StateStore {

    mqttBroker = null;
    mqttUsername = null;
    mqttPassword = null;
    mqttClientId = null;
    mqttTopic = null;
    mqttLoadTimeout=null;
    mqttClient = null;
    mqttInFlight = 0;
    mqttSubscribed = false;

    //Overide this and call super()
    constructor(options,recordProcessor) {
        super(options,recordProcessor);
        this.mqttBroker = options.mqtt.broker;
        this.mqttUsername = options.mqtt.username;
        this.mqttPassword = options.mqtt.password;
        this.mqttClientId = options.mqtt.clientId;
        this.mqttTopic = options.mqtt.topic;
        this.mqttLoadTimeout=options.mqtt.loadTimeout||1000;
        
    }

    //Override these and do not call super();
    //Write persisted messages to store, Could be buffered. Buffer is written with _purge()
    async _processPersistCommand(key, record, persistToken) {
        const topicParts = this.mqttTopic.split("/");
        topicParts.pop();
        topicParts.push(key);
        const topic = topicParts.join("/");

        let message = null;
        if (record) message = JSON.stringify(record);
        this.mqttInFlight++;
        //await new Promise((resolve, reject) => {
        this.mqttClient.publish(topic, message, { qos: 1, retain: true }, async (err,result) => {
            if (!err) {
                this.mqttInFlight--;
                await this._recordPersisted(persistToken);

            } else
                throw (err);
        });
    }

    //Flush unwritten records to store
    async _purge() {
        // No need will resolve when _recordPersisted() has been called for all in flight records.
    }

    //Connect and subscribe to store. Pull or push. Setup timers
    async _subscribe() {
        const self = this;
        let lastMessageTimestamp = null;

        this.mqttClient.on('message', async function (topic, message) {
            lastMessageTimestamp = new Date();
            const topicParts = topic.split("/");
            const key = topicParts[topicParts.length - 1];
            const entity = JSON.parse(message);
            await self._queueRecord(key, entity);
        });


        await new Promise((resolve, reject) => {
            this.mqttClient.subscribe(this.mqttTopic, {qos: 1}, function (err) {
                if (!err) {
                    lastMessageTimestamp = new Date();
                    resolve();
                } else
                    reject(err);
            });
        });

        await async.whilst(
            async () => !shared.isOlderThan(lastMessageTimestamp, 1000),
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });

        await this._loaded();
    }

    //Unsubscribe and disconnect from store. Clear timers.
    async _unsubscribe() {
        await new Promise((resolve, reject) => {
            this.mqttClient.unsubscribe(this.mqttTopic, {}, function (err) {
                if (!err) {
                    resolve();
                } else
                    reject(err);
            });
        });

    }

    async _connect() {
        this.mqttClient = MqttClient.connect(this.mqttBroker, {
            username: this.mqttUsername,
            password: this.mqttPassword,
            clientId: this.mqttClientId
        });

        await new Promise((resolve, reject) => {
            this.mqttClient.on('connect', function (result, err) {
                if (!err) {
                    resolve();
                } else
                    reject(err);
            });
        });
    }


    async _disconnect() {
        this.mqttClient.end();
    }

}

module.exports = StateStoreMQTT;