const StateStore = require('./statestore');
const MqttClient = require("mqtt");
const async = require("async");
const shared = require("./shared");

class StateStoreMQTT extends StateStore {

    #mqttClient = null;
    #mqttBroker = null;
    #mqttUsername = null;
    #mqttPassword = null;
    #mqttClientId = null;
    #mqttTopic = null;
    #pushIntervalSec = null;
    #pushTimeout = null;
    #lastPolledKeys = [];
    #waitingChanges = [];
    #persistToken = 0;
    #inFlight=0;


    constructor(options) {
        super(options);

        //Validate StateStoreJSON options
        if (!options.mqttBroker) throw ("State store (MQTT) mqttBroker option to construct object");
        if (!options.mqttUsername) throw ("State store (MQTT) mqttUsername option to construct object");
        if (!options.mqttPassword) throw ("State store (MQTT) mqttPassword option to construct object");
        if (!options.mqttTopic) throw ("State store (MQTT) mqttTopic option to construct object");
        this.#mqttBroker = options.mqttBroker;
        this.#mqttUsername = options.mqttUsername || null;
        this.#mqttPassword = options.mqttPassword || null;
        this.#mqttClientId = options.mqttClientId || null;
        this.#mqttTopic = options.mqttTopic || null;
    };

    async load() {
        await super.load([]);
        const loadedState = [];
        this.#mqttClient = MqttClient.connect(this.#mqttBroker, {
            username: this.#mqttUsername,
            password: this.#mqttPassword,
            clientId: this.#mqttClientId
        })

        const self = this;
        let lastMessageTimestamp = null;

        this.#mqttClient.on('message', async function (topic, message) {
            lastMessageTimestamp = new Date();
            const topicParts = topic.split("/");
            const key = topicParts[topicParts.length - 1];
            const entity = JSON.parse(message);
            loadedState.push({ key: key, entity: entity });
        });


        await new Promise((resolve, reject) => {
            this.#mqttClient.on('connect', function () {
                self.#mqttClient.subscribe(self.#mqttTopic, {}, function (err) {
                    if (!err) {
                        lastMessageTimestamp = new Date();
                        resolve();
                    } else
                        reject(err);
                })
            });
        });

        await async.whilst(
            async () => !shared.isOlderThan(lastMessageTimestamp, 2000),
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });

        await new Promise((resolve, reject) => {
            self.#mqttClient.unsubscribe(self.#mqttTopic, {}, function (err) {
                if (!err) {
                    lastMessageTimestamp = new Date();
                    resolve();
                } else
                    reject(err);
            });
        });

        await async.eachOfLimit(loadedState, 1,
            async (entity) => {
                await super.recieve(entity.key, entity.entity);
            });
        return loadedState.length;
    };

    async persist(key, entityRecord) {
        const persistToken = this.#persistToken;
        this.#persistToken++;
        //this.#waitingChanges.push({ key: key, entity: entityRecord, persistToken: persistToken });
        const topicParts=this.#mqttTopic.split("/");
        topicParts.pop();
        topicParts.push(key);
        const topic=topicParts.join("/");
        new Promise(async (resolve1, reject1) => {
            await new Promise((resolve, reject) => {
                this.#inFlight++;
                let message=null;
                if(entityRecord) message=JSON.stringify(entityRecord);
                this.#mqttClient.publish(topic,message , {qos:2, retain:true}, (err) => {
                    if (!err) {
                        this.#inFlight--;
                        resolve();
                    } else
                        reject(err);
                });
            });

            this.emit("persisted", { key: key, persistToken: persistToken });
            await super.recieve(key, entityRecord);
            resolve1();
        });

        return { key: key, persistToken: persistToken };;
    };

    async close() {
        const result = await super.close();
        this.#mqttClient.end();
        return result;
    };

    async purge() {
        await async.whilst(
            async () => this.#inFlight,
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });
    };

    
}

module.exports = StateStoreMQTT;