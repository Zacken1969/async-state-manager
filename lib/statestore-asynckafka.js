const StateStore = require('./statestore');
//const AsyncKafka = require("async-kafka");
const async = require("async");
const shared = require("./shared");

class StateStoreMQTT extends StateStore {

    #kafkaProducer = null;
    #kafkaConsumer = null;
    #kafkaTopic = null;
    #inFlight = 0;
    #didConnect = false;


    constructor(options) {
        super(options);

        //Validate StateStoreJSON options
        if (!options.kafkaProducer) throw ("State store (Async-Kafka) need a producer construct object");
        if (!options.kafkaConsumer) throw ("State store (Async-Kafka) need a producer construct object");
        if (!options.kafkaTopic) throw ("State store (Async-Kafka) need a state topic construct object");
        this.#kafkaProducer = options.kafkaProducer;
        this.#kafkaConsumer = options.kafkaConsumer;
        this.#kafkaTopic = options.kafkaTopic;
    };

    async load() {
        await super.load([]);
        let loadedEntities = 0;
        this.#didConnect = false;
        if (!this.#kafkaConsumer.isConnected()) {
            await this.#kafkaConsumer.connect();
            this.#didConnect = true;
        }
        await this.#kafkaConsumer.subscribe([{
            topic: this.#kafkaTopic,
            decoderType: 2,
            consumeMode: 1,
            autoCommit: false,
            keepOffsetOnReassign: true,
            processor: async (message, kafkaHeader) => {
                await super.recieve(kafkaHeader.key, message);
                loadedEntities++;
            }
        }
        ]);

        await this.#kafkaConsumer.unsubscribe([this.#kafkaTopic]);


        return loadedEntities;
    };

    async persist(key, entityRecord) {
        this.#inFlight++;
        let sendToken = await this.#kafkaProducer.send(
            this.#kafkaTopic,
            entityRecord,
            key,
            async (deliveryToken) => {
                this.#inFlight--;
                this.emit("persisted", { key: key, persistToken: deliveryToken });
                await super.recieve(key, entityRecord);
            },
            2,
            null
        );
        return { key: key, persistToken: sendToken };
    };

    async close() {
        const result = await super.close();
        if (this.#didConnect && !this.#kafkaConsumer.isSubscribed()) await this.#kafkaConsumer.disconnect();
        return result;
    };

    async purge() {
        await async.whilst(
            async () => this.#inFlight,
            async () => {
                await new Promise((resolve, reject) => setImmediate(() => resolve()));
            });
    };


}

module.exports = StateStoreMQTT;